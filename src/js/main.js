jQuery(document).ready(function($) {
	// Animations
	//-----------------------------------------------
	var delay=0, setTimeoutConst;
	if (($("[data-animation-effect]").length>0) && !Modernizr.touch) {
		$("[data-animation-effect]").each(function() {
			var item = $(this),
			animationEffect = item.attr("data-animation-effect");

			if(Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
				item.appear(function() {
					if(item.attr("data-effect-delay")) item.css("effect-delay", delay + "ms");
					setTimeout(function() {
						item.addClass('animated object-visible ' + animationEffect);

					}, item.attr("data-effect-delay"));
				}, {accX: 0, accY: -130});
			} else {
				item.addClass('object-visible');
			}
		});
	};

	// PieChart
	//-----------------------------------------------
	$(".progress-pie-chart").each(function() {
		var $ppc = $(this),
		percent = parseInt($ppc.data('percent')),
		deg = 360*percent/100;
		if (percent > 99) {
			$ppc.addClass('gt-50');
		}
		$ppc.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
		$ppc.find('.ppc-percents span.percent').html(percent+'%');
	});


	// Map highlights
	//-----------------------------------------------
	$("#map_menu ul li").hover(
		function() {
			var target = $(this).attr('data-edtarget');
			console.log(target)
			$(".map_img").addClass(target);
		}, function() {
			var target = $(this).attr('data-edtarget');
			$(".map_img").removeClass(target);
		}
	)

	// Quiz start
	//-----------------------------------------------
	$("[data-item='question']").not("[data-index='1']").hide();

	// Select answer
	//-----------------------------------------------
	$(".answer").click(function() {
		var me = $(this);
		var correct = me.attr('data-correct');

		if (correct == "true") {
			me.addClass('selected unable');
			me.siblings('.answer').addClass('unable');
			me.parent().next().find('.nextBtn').removeClass('disabled')
		} else if (correct == "false") {
		}
	});

	
});

// Change stage
//-----------------------------------------------
function changeStage() {
	$("#intro").addClass("toggleUp").removeClass("toggleDown");
	$("#map").addClass("toggleDown").removeClass("toggleUp");
	setTimeout(function() {
		$("#map_menu").removeClass('object-non-visible hidden').addClass('animated object-visible fadeIn')
	}, 300)
}

// Change question
//-----------------------------------------------
function changeQuestion(key) {
	var actual = $("[data-item='question'].active");

	if (key == "next") {
		var next = actual.next("[data-item='question']");

		actual.removeClass('active fadeIn').addClass('animated fadeOut');
		setTimeout(function() {
			actual.hide();
			next.addClass('animated fadeIn').removeClass('fadeOut').show();
		}, 400);
		next.addClass('active');

	} else if (key == "prev") {
		var prev = actual.prev("[data-item='question']");

		actual.removeClass('active fadeIn').addClass('animated fadeOut');
		setTimeout(function() {
			actual.hide();
			prev.addClass('animated fadeIn').removeClass('fadeOut').show();
		}, 400);
		prev.addClass('active');
	}
}